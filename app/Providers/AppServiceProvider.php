<?php

namespace App\Providers;

use App\View\Components\Vue;
use App\View\Components\Layouts;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        app()->register(ReactiveServiceProvider::class);
    }

    public function boot(): void
    {
        $this->bootBlade();
    }

    public function bootBlade(): void
    {
        Blade::componentNamespace(Layouts::class, 'layouts');
        Blade::componentNamespace(Vue::class, 'vue');
    }
}
