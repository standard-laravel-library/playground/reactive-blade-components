<?php

namespace App\Providers;

use Illuminate\View;
use App\View\Factory;
use App\View\ReactiveManager;
use Illuminate\View\Component;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class ReactiveServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->registerView();
    }

    public function registerBladeComponentMechanisms(): void
    {
        Component::resolveComponentsUsing([ReactiveManager::class, 'resolve']);
    }

    public function registerView(): void
    {
        $this->registerViewMacros();
        $this->registerBladeComponentMechanisms();
        app()->singleton('view', fn () => Factory::instantiate()); // TODO: This is not ideal as any other package that may bind a different view definition to the container will break the system
    }

    public function registerViewMacros(): void
    {
        View\Factory::macro('pushComponent', function (View\View $view, iterable $data = []) {
            $this->componentStack[] = $view;

            $this->componentData[$this->currentComponent()] = $data;

            $this->slots[$this->currentComponent()] = [];

            return $this;
        });
    }

    public function boot(): void
    {
        $this->bootRoute();
    }

    public function bootRoute(): void
    {
        Route::bind('reactiveId', fn ($reactiveId) => unserialize(cache()->get($reactiveId)));
    }
}
