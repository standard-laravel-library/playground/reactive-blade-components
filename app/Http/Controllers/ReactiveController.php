<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ReactiveComponentResource;

class ReactiveController extends Controller
{
    public function __invoke(Request $request, $fingerprint)
    {
        $component = data_get($fingerprint, 'component');

        $component->hydrate($request->toArray());
        $component->call(data_get($request, 'call.method'), data_get($request, 'call.parameters'));
        $component->preserveDefaultSlot(content: $fingerprint->get('slot'));

        return new ReactiveComponentResource($component, $fingerprint);
    }
}
