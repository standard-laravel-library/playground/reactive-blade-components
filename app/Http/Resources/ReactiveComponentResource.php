<?php

namespace App\Http\Resources;

use Closure;
use Illuminate\View\Factory;
use Illuminate\View\Component;
use Illuminate\Support\Collection;
use Illuminate\Contracts\View\View;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Http\Resources\Json\JsonResource;

class ReactiveComponentResource extends JsonResource
{
    protected Component $component;

    protected Collection $fingerprint;

    public function __construct(Component $component, Collection $fingerprint)
    {
        $this->component = $component;
        $this->fingerprint = $fingerprint;
    }

    public function toArray($request)
    {
        return  [
            'data' => $this->component->dehydrate(),
            'html' => resolve(Factory::class)->pushComponent(
                $this->view(),
                $this->data()
            )->renderComponent(),
        ];
    }

    protected function view(): View|Htmlable|Closure|string
    {
        return $this->component->resolveView();
    }

    protected function data(): array
    {
        return $this->fingerprint->merge($this->component->data())->toArray();
    }
}
