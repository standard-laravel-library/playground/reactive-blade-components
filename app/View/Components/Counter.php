<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\View\Components\Concerns\Reactive;

class Counter extends Component
{
    use Reactive;

    // TODO: public properties are 2 way reactive?
    // TODO: protected properties are 1 way reactive?
    // TODO: private properties are not reactive?
    public int $count;

    public bool $open = false;

    public function __construct(int $count)
    {
        $this->count = $count;
    }

    public function decrement()
    {
        $this->count--;
    }

    public function increment()
    {
        $this->count++;
    }

    public function set(int $number)
    {
        $this->count = $number;
    }

    public function toggle()
    {
        $this->open = ! $this->open;
    }

    public function render()
    {
        return view('components.counter');
    }
}
