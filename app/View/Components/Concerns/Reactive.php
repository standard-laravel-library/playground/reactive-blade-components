<?php

namespace App\View\Components\Concerns;

use ReflectionClass;
use ReflectionProperty;
use Illuminate\Support\Str;
use Illuminate\View\Component;
use Illuminate\Support\Collection;

// TODO: Is this true still? Attributes are not retained on subsequent renders
// TODO: Is this true still? Slots are not retained on subsequent page loads
trait Reactive
{
    public ?string $reactiveId = null;

    public ?Component $component = null;

    public function mountReactive(): void
    {
        $this->ensureReactiveIdentifier();
        $this->provideComponentToView();
    }

    public function hydrate(array $data): void
    {
        $this->properties()->each(
            fn ($property) => $this->$property = data_get($data, $property)
        );
    }

    public function dehydrate(): Collection
    {
        return $this->properties()->mapWithKeys(fn ($property) => [$property => $this->$property]);
    }

    public function call(string $method, array $parameters): mixed
    {
        return $this->$method(...$parameters);
    }

    public function preserveDefaultSlot(mixed $content)
    {
        if ($content && ob_start()) {
            echo (string) $content;
        }
    }

    public function withAttributes(array $attributes): static
    {
        parent::withAttributes($attributes);

        return $this;
    }

    public function methods(): Collection
    {
        return  static::$methodCache[static::class];
    }

    public function properties(): Collection
    {
        $reflection = new ReflectionClass($this);

        return collect($reflection->getProperties(ReflectionProperty::IS_PUBLIC))
            ->reject(function (ReflectionProperty $property) {
                return $property->isStatic();
            })
            ->reject(function (ReflectionProperty $property) {
                return $this->shouldIgnore($property->getName());
            })
            ->filter(function (ReflectionProperty $property) {
                return $property->class == static::class;
            })
            ->map(function (ReflectionProperty $property) {
                return $property->getName();
            })->reject(fn ($name) => $name == 'component');
    }

    public function mountTraits(): static
    {
        $class = static::class;

        collect(class_uses_recursive($class))->map(
            fn ($trait) => 'mount' . class_basename($trait)
        )->filter(
            fn ($method) => method_exists($this, $method)
        )->each(
            fn ($method) => $this->{$method}()
        );

        return $this;
    }

    public function cache(): static
    {
        cache()->set($this->reactiveId, serialize($this));

        return $this;
    }

    protected function ensureReactiveIdentifier(): static
    {
        if (! $this->reactiveId) {
            $this->reactiveId = class_basename(static::class) . '_' . Str::random();
        }

        return $this;
    }

    protected function provideComponentToView(): static
    {
        $this->component = $this;

        return $this;
    }
}
