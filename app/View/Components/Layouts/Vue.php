<?php

namespace App\View\Components\Layouts;

use Illuminate\View\Component;

class Vue extends Component
{
    public function render()
    {
        return view('layouts.vue');
    }
}
