<?php

namespace App\View\Components\Layouts;

use Illuminate\View\Component;

class Standard extends Component
{
    public function render()
    {
        return view('layouts.standard');
    }
}
