<?php

namespace App\View\Components\Vue;

use Closure;
use Illuminate\View\Component;
use Illuminate\Contracts\View\View;

class Counter extends Component
{
    public int $count = 0;

    public function __construct(int $count = 0)
    {
        $this->count == $count;
    }

    public function render(): View | Closure | string
    {
        return view('vue.counter');
    }
}
