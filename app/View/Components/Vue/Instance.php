<?php

namespace App\View\Components\Vue;

use Closure;
use Illuminate\View\Component;
use Illuminate\Contracts\View\View;

class Instance extends Component
{
    public function render(): View | Closure | string
    {
        return view('vue.instance');
    }
}
