<?php

namespace App\View;

use Closure;
use Illuminate\View;
use Illuminate\View\Component;
use Illuminate\View\InvokableComponentVariable;

class Factory extends View\Factory
{
    public function renderComponent()
    {
        $output = parent::renderComponent();

        tap(collect($this->currentComponentData), function ($data) {
            $serialized = serialize(
                $data->reject(
                    fn ($item, $key) => $item instanceof InvokableComponentVariable || $item instanceof Closure || collect(['resolve', 'resolveComponentsUsing'])->contains($key)
                )
            );
            cache()->set($data->get('reactiveId'), $serialized);
        });

        return $output;
    }

    public static function instantiate()
    {
        $resolver = resolve('view.engine.resolver');

        $finder = resolve('view.finder');

        $factory = new Factory($resolver, $finder, resolve('events'));

        $factory->setContainer(app());

        $factory->share('app', app());

        app()->terminating(static function () {
            Component::forgetFactory();
        });

        return $factory;
    }
}
