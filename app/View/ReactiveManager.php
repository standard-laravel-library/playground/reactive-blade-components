<?php

namespace App\View;

use App\View\Components\Concerns\Reactive;

class ReactiveManager
{
    public static function resolve($class, $data)
    {
        $component = static::initialize($class, $data);
        $component = static::mount($component);

        return $component;
    }

    protected static function initialize($class, $data)
    {
        $class::forgetComponentsResolver();
        $component = $class::resolve($data);
        $class::resolveComponentsUsing([static::class, 'resolve']);

        return $component;
    }

    protected static function mount($component)
    {
        if (collect(class_uses_recursive($component))->doesntContain(Reactive::class)) {
            return $component;
        }

        $component->mountTraits();

        return $component;
    }
}
