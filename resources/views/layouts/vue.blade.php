
<x-layouts::base>
    <x-slot name="headScripts">
        <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
    </x-slot>

    {{ $slot }}

    <x-slot name="scripts">
        <x-vue::instance />
    </x-slot>

</x-layouts::base>
