
<x-layouts::base>
    <x-slot name="headScripts">
        <script defer src="https://unpkg.com/@alpinejs/morph@3.x.x/dist/cdn.min.js"></script>
        <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
    </x-slot>

    {{ $slot }}
</x-layouts::base>
