<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Laravel</title>

        <script src="https://cdn.tailwindcss.com"></script>

        <style>
            [x-cloak] { display: none !important; }
        </style>

        {{ $headStyles ?? null }}
        @stack('head:styles')

        {{ $headScripts ?? null }}
        @stack('head:scripts')
    </head>
    <body class="antialiased">
        <div id="app" class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
            <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
                {{ $slot }}
            </div>
        </div>

        {{ $scripts ?? null }}
        @stack('scripts')
    </body>
</html>
