@push('scripts')
    <script>
        const { createApp } = Vue;

        const VueApp = createApp({});
    </script>

    @stack('vue-components')

    <script>
        VueApp.mount('#app');
    </script>
@endpush
