@push('vue-components')
    <script>
        VueApp.component('counter', {
            template: `
                <div>
                    <div class="flex-col text-center space-y-4">
                        <div class="flex-col space-y-8">
                            <div class="flex">
                                <button
                                    @click="count--"
                                    class="translate-x-12 bg-red-100 border-red-300 border-2 text-gray-600 p-12 rounded-full h-20 w-20 flex items-center justify-center"
                                >
                                    <span class="block font-bold"> - </span>
                                </button>
                                <div class="block bg-blue-100 p-12 h-64 w-64 flex items-center justify-center rounded-full text-2xl font-bold">
                                    <span>
                                        @{{ count }}
                                    </span>
                                </div>
                                <button
                                    class="-translate-x-12 bg-green-100 border-green-300 border-2 text-gray-600 p-12 rounded-full h-20 w-20 flex items-center justify-center"
                                    @click="count++"
                                >
                                    <span class="block font-bold"> + </span>
                                </button>
                            </div>
                        </div>
                        <div class="flex items-center justify-center mx-auto">
                            <input type="text" v-model="count" class="block px-3 py-2 rounded">
                            <button @click="" class="px-3 py-2 bg-gray-300 text-gray-800 rounded">Set</button>
                        </div>
                    </div>
                        @{{ random }}
                        <slot></slot>
                </div>
            `,
            data() {
                return {
                    count: {{ $count }}
                }
            },
            computed: {
                random() {
                    return Math.random(0, this.count)
                }
            }
        });
    </script>
@endpush

<counter :count="0">
    <counter :count="0" />
</counter>
