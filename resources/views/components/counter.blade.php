<x-reactive :component="$component">
    <div {{ $attributes }}>
        <div class="flex-col text-center space-y-4">
            <div class="flex-col space-y-8">
                <div class="flex">
                    <button
                        @click="decrement()"
                        class="translate-x-12 bg-red-100 border-red-300 border-2 text-gray-600 p-12 rounded-full h-20 w-20 flex items-center justify-center"
                    >
                        <span class="block font-bold"> - </span>
                    </button>
                    <div class="block bg-blue-100 p-12 h-64 w-64 flex items-center justify-center rounded-full text-2xl font-bold">
                        <span>
                            {{ $count }}
                        </span>
                    </div>
                    <button
                        class="-translate-x-12 bg-green-100 border-green-300 border-2 text-gray-600 p-12 rounded-full h-20 w-20 flex items-center justify-center"
                        @click="increment()"
                    >
                        <span class="block font-bold"> + </span>
                    </button>
                </div>
            </div>
            <div class="flex items-center justify-center mx-auto">
                <input type="text" x-model="count" class="block px-3 py-2 rounded">
                <button @click="set(count)" class="px-3 py-2 bg-gray-300 text-gray-800 rounded">Set</button>
            </div>
        </div>

        @unless($slot->isEmpty())
            <div class="flex justify-center"><button @click="toggle()" class="px-3 py-2 bg-yellow-300 text-gray-800 rounded mt-4">Toggle</button></div>

            <div x-show="open" x-cloak class="scale-75 mt-4 text-center">
                <span class="text-bold">Default Slot</span>
                {{ $slot }}
            </div>
        @endempty
        {{ $named ?? null }}
    </div>
</x-reactive>
