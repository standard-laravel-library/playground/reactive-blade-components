@props([
    'component'
])

<div x-data="{{ $component->reactiveId }}" data-reactive-id="{{ $component->reactiveId }}">
    {{ $slot }}
</div>

@push('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('{{ $component->reactiveId }}', () => ({
                @foreach($component->properties() as $property)
                    {{ $property }}: {!! json_encode(data_get($component, $property)) !!}@if(!$loop->last),@endif
                @endforeach,
                fetch(type, method, parameters) {
                    let element = document.querySelector('[data-reactive-id="{{ $component->reactiveId }}"]');
                    fetch(
                        '/reactive/{{ $component->reactiveId }}',
                        {
                            method: type,
                            headers: {
                                "Content-Type": 'application/json',
                                "Accept": 'application/json',
                                "X-CSRF-TOKEN": '{{ csrf_token() }}'
                            },
                            body: JSON.stringify({
                                @foreach($component->properties() as $property)
                                    {{ $property }}: this.{{ $property}}@if(!$loop->last),@endif
                                @endforeach,
                                call: {
                                    method,
                                    parameters
                                }
                            })
                        }
                    ).then((response) => response.json()).then(
                        (data) => {
                            @foreach($component->properties() as $property)
                                this.{{ $property }} = data.data.{{ $property }};
                            @endforeach
                            Alpine.morph(
                                document.querySelector('[data-reactive-id="{{ $component->reactiveId }}"]'),
                                data.html,
                                {
                                    updating(from, to, childrenOnly, skip) {
                                        if(to.nodeType !== Node.ELEMENT_NODE) return;
                                        let reactiveId = to.getAttribute('data-reactive-id');
                                        if(reactiveId && reactiveId !== '{{ $component->reactiveId }}') {
                                            skip();
                                        }
                                    }
                                }
                            );
                        }
                    );
                },
                @foreach($component->methods() as $method)
                    {{$method}}(...parameters) {
                        this.fetch('POST', '{{ $method}}', parameters);
                    }@if(!$loop->last),@endif
                @endforeach
            }));
        });
    </script>
@endpush
